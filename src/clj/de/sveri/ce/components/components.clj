(ns de.sveri.ce.components.components
  (:require
    [com.stuartsierra.component :as component]
    [de.sveri.ce.components.server :refer [new-web-server]]
    [de.sveri.ce.components.handler :refer [new-handler]]
    [de.sveri.ce.components.config :as c]
    [de.sveri.ce.components.state :as state]
    [de.sveri.ce.components.scheduler :as sched]
    [de.sveri.ce.components.bitfinex :as bitf]
    [de.sveri.ce.components.bot-bitfinex :as bot-bit]
    [de.sveri.ce.components.db :refer [new-db]]))


(defn dev-system []
  (component/system-map
    :config (c/new-config (c/prod-conf-or-dev))
    :state (component/using (state/new-state) [:config])
    :db (component/using (new-db) [:config])
    :scheduler (component/using (sched/new-scheduler) [:db])
    :bitfinex (component/using (bitf/new-bitfinex) [:config :db])
    :bitfinex-bot (component/using (bot-bit/new-bot-bitfinex) [:config :db :state])
    :handler (component/using (new-handler) [:config :db])
    :web (component/using (new-web-server) [:handler :config])))


(defn prod-system []
  (component/system-map
    :config (c/new-config (c/prod-conf-or-dev))
    :state (component/using (state/new-state) [:config])
    :db (component/using (new-db) [:config])
    :scheduler (component/using (sched/new-scheduler) [:db])
    :bitfinex (component/using (bitf/new-bitfinex) [:config :db])
    :bitfinex-bot (component/using (bot-bit/new-bot-bitfinex) [:config :db :state])
    :handler (component/using (new-handler) [:config :db])
    :web (component/using (new-web-server) [:handler :config])))
