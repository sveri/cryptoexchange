(ns de.sveri.ce.remoteapi.bitfinex-trade
  (:require [de.sveri.ce.db.bitfinex :as db-bit]
            [de.sveri.ce.service.bitfinex :as s-bit]
            [clojure.data.json :as json]
            [gniazdo.core :as ws]
            [clj-time.core :as time-c]
            [clj-time.coerce :as time-coer]))

(def sockets (atom {}))

(def loc-db {:connection-uri "jdbc:postgresql://localhost:5432/cryptoexchange?user=cryptoexchange&password=cryptoexchange"})

(defn get-sorted-history-orders-last-order-first [{:keys [personal-data]}] (reverse (sort-by :id (:order-history @personal-data))))

(defn filter-executed-orders [orders] (filter #(.startsWith (:status %) "EXECUTED") orders))

(defn get-buys [orders] (filter #(< 0 (:amount %)) (filter-executed-orders orders)))

(defn get-sells [orders] (filter #(< (:amount %) 0) (filter-executed-orders orders)))

(defn get-last-executed-buy-price [state]
  (:price (first (get-buys (get-sorted-history-orders-last-order-first state)))))

(defn get-last-executed-buy-amount [state]
  (:amount (first (get-buys (get-sorted-history-orders-last-order-first state)))))


(defn get-wallet [wallets wallet-type]
  (->> wallets
       (filter #(= wallet-type (:currency %)))
       first))

(defn buy-or-sell [wallets]
  (let [usd-wallet (get-wallet wallets "USD")]
    (if (< 20 (:amount usd-wallet)) "buy" "sell")))

(defn get-buy-or-sell-price [{:keys [margin] :as state} buy-or-sell db]
  (let [trades (db-bit/get-trades db 3)
        quantiles (s-bit/get-quantiles trades)
        buy-quant (:quantile-buy-price quantiles)
        sell-quant (:quantile-sell-price quantiles)]
    (if (= "sell" buy-or-sell) (+ @margin (get-last-executed-buy-price state))
                               (nth buy-quant 2))))


(defn get-cur-total-balance [{:keys [personal-data] :as state} db]
  (let [cur-sell-price (nth (:quantile-sell-price (s-bit/get-quantiles (db-bit/get-trades db 3))) 2)
        sell-amount (:amount (get-wallet (:wallets @personal-data) "BTC"))
        cur-dollar-amount (:amount (get-wallet (:wallets @personal-data) "USD"))]
    (+ cur-dollar-amount (* sell-amount cur-sell-price))))


(defn make-new-order [{:keys [personal-data btc-amount-to-trade] :as state} db]
  (let [wallets (:wallets @personal-data)]
    (if (empty? wallets)
      (println "cannot make new order because wallet info is missing")
      (let [buy-or-sell (buy-or-sell wallets)
            price (int (get-buy-or-sell-price state buy-or-sell db))
            price-str (str price)
            btc-wallet (get-wallet wallets "BTC")
            amount (str (if (= "sell" buy-or-sell) (* -1 (:amount btc-wallet)) @btc-amount-to-trade))
            order [0, "on", nil
                   {:cid (System/currentTimeMillis) :type "EXCHANGE LIMIT" :symbol "tBTCUSD" :amount amount :price price-str :hidden 0}]]
        (println (json/write-str order))
        (ws/send-msg (:secured-bitfinex-socket @sockets) (json/write-str order))))))


(defn is-buy-order? [amount] (< 0 amount))
(defn is-sell-order? [amount] (not (is-buy-order? amount)))



(defn cancel-order [order]
  (let [order [0, "oc", nil {:id (:id order)}]]
    (println (str "cancel order: " (json/write-str order)))
    (ws/send-msg (:secured-bitfinex-socket @sockets) (json/write-str order))))

(defn hit-1-percent-rule? [{:keys [personal-data] :as state} db]
  (let [last-buy-price (get-last-executed-buy-price state)
        last-amount (get-last-executed-buy-amount state)
        last-foreign-buy-price  (:price (first (db-bit/get-last-buy-trade db)))
        last-buy-value (* last-buy-price last-amount)
        cur-buy-value (* last-foreign-buy-price last-amount)
        one-percent (* 0.01 last-buy-value)]
    (< cur-buy-value (- last-buy-value one-percent))))

(defn possibly-update-current-order [{:keys [personal-data] :as state} db]
  (let [first-order (first (:orders @personal-data))
        order-timestamp (:created first-order)
        now (time-coer/to-long (time-c/now))]
    (when (and (is-buy-order? (:amount first-order)) (< 0 (- now (+ order-timestamp 60000))))
      (cancel-order first-order))
    (when (and (is-sell-order? (:amount first-order)) (hit-1-percent-rule? state db))
      (cancel-order first-order))))

(def trading-allowed (atom false))


; TODO calculate amount of BCs to buy
; update order instead of canceling
(defn trade [{:keys [personal-data] :as state} db]

  (when @trading-allowed
    (if (empty? (:orders @personal-data))
      (make-new-order state db)
      (possibly-update-current-order state db))))
