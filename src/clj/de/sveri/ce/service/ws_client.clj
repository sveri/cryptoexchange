(ns de.sveri.ce.service.ws-client
  (:require [immutant.web.async :as async]
            [clojure.tools.logging :as log]
            [cognitect.transit :as t]
            [de.sveri.ce.remoteapi.bitfinex :as bitf-api]
            [de.sveri.ce.service.state :as state]
            [clojure.edn :as edn]
            [clojure.data.json :as json])
  (:import (java.io ByteArrayOutputStream)))



(defn connect! [channel]
  (log/info "channel open")
  (swap! state/channels conj channel))

(defn disconnect! [channel {:keys [code reason]}]
  (log/info (str "close code:" code "reason:" reason))
  (swap! state/channels #(remove #{channel} %)))

(defn notify-clients! [channel msg]
  (let [msg (json/read-str msg :key-fn keyword)]))
    ;(when (= "init" (:type msg))
    ;  (doseq [channel @state/channels]
    ;    (async/send! channel (state/encode-transit {:type :wallets :wallets (:wallets @bitf-api/personal-data)}))))))



(def websocket-callbacks
  "WebSocket callback functions"
  {:on-open    connect!
   :on-close   disconnect!
   :on-message notify-clients!})
