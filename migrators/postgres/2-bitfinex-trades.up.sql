CREATE TABLE bitfinex_trades (
id bigserial NOT NULL PRIMARY KEY,
trade_id character varying(30) NOT NULL ,
traded_at timestamp without time zone NOT NULL,
action character varying(10) NOT NULL,
amount NUMERIC(20, 15),
price NUMERIC(20, 10)
);

ALTER TABLE bitfinex_trades OWNER TO cryptoexchange;
