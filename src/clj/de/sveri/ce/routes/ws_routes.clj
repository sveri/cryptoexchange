(ns de.sveri.ce.routes.ws-routes
  (:require [immutant.web.async :as async]
            [compojure.core :refer [routes GET POST]]
            [clojure.tools.logging :as log]
            [de.sveri.clojure.commons.log :as l]
            [de.sveri.ce.service.ws-client :as ws-c]))






(defn ws-handler [request]
  (async/as-channel request ws-c/websocket-callbacks))

(defn ws-routes []
  (routes
    (GET "/ws" [] ws-handler)))