(ns de.sveri.ce.remoteapi.bitfinex
  (:require [de.sveri.ce.components.config :as conf]
            [clj-http.client :as cl]
            [clojure.data.json :as json]
            [clojure.java.jdbc :as j]
            [immutant.web.async :as async]
            [cognitect.transit :as t]
            [clojure.tools.logging :as l]
            [de.sveri.ce.service.state :as state]
            [de.sveri.ce.service.bitfinex :as s-bit]
            [de.sveri.ce.db.bitfinex :as db-bit]
            [clj-time.core :as time-c]
            [clj-time.coerce :as time-coer]
            [gniazdo.core :as ws])
  (:import (java.sql Timestamp)))




(defn is-heartbeat? [msg]
  (= "hb" (second (json/read-str msg))))

(defn is-no-heartbeat? [msg]
  (not (is-heartbeat? msg)))

(defn insert-trade-into-db [db trade-details]
  (j/insert! db :bitfinex_trades (dissoc
                                   (assoc trade-details :trade_id (:trade-id trade-details) :traded_at (:traded-at trade-details))
                                   :trade-id :traded-at :type)))



(defn handle-trades-msg [db msg]
  (let [trade (json/read-str msg)]
    (when (= "tu" (second trade))
      (let [trade-details (nth trade 2)
            id (first trade-details)
            timestamp (second trade-details)
            action (if (neg? (nth trade-details 2)) :sell :buy)
            amount (Math/abs (nth trade-details 2))
            price (nth trade-details 3)
            trade-data {:trade-id id :traded-at (new Timestamp timestamp) :action (name action)
                        :amount amount :price price :type :trade}]
        (insert-trade-into-db db trade-data)
        ;(l/info "----------------")
        ;(l/info trade-data)
        (doseq [channel @state/channels]
          (async/send! channel (state/encode-transit trade-data)))))))




(defn convert-ticker-msg-to-ticker-data [msg]
  (let [ticker-data (second (json/read-str msg))]
    {:bid (nth ticker-data 0 0) :bid-size (nth ticker-data 1 0) :ask (nth ticker-data 2 0) :ask-size (nth ticker-data 3 0)
     :daily-change (nth ticker-data 4 0) :daily-change-perc (nth ticker-data 5 0) :last-price (nth ticker-data 6 0)
     :volume (nth ticker-data 7 0) :high (nth ticker-data 8 0) :low (nth ticker-data 9 0)
     :exchange-name "Bitfinex" :type :ticker}))

(defn handle-ticker-msg [db msg]
  (when (is-no-heartbeat? msg)
    (let [ticker-data (convert-ticker-msg-to-ticker-data msg)]
      (doseq [channel @state/channels]
        (async/send! channel (state/encode-transit ticker-data))))))


;---------------




;[0 ws [[exchange BTC 6.3E-5 0 nil] [exchange USD 103.93727812 0 nil]]]
(defn parse-wallets [msg-data {:keys [personal-data]}]
  (println "parse wallets: " msg-data)
  (let [wallets (map (fn [wallet] {:type (nth wallet 0) :currency (nth wallet 1) :amount (nth wallet 2)})
                     (nth msg-data 2))]
    (swap! personal-data assoc-in [:wallets] wallets)
    {:type wallets :wallets wallets}))


;[0 wu [exchange BTC 0.037 0 nil]]
(defn parse-wallet-update [msg-data {:keys [personal-data]}]
  (println "wallet update: " msg-data)
  (let [update-wallet (nth msg-data 2)
        wallets-from-state (:wallets @personal-data)
        update-wallet-currency (second update-wallet)
        update-wallet-data {:type "exchange" :currency update-wallet-currency :amount (nth update-wallet 2)}
        new-wallets (conj
                      (remove #(= (:currency %) update-wallet-currency) wallets-from-state)
                      update-wallet-data)]
    (swap! personal-data assoc-in [:wallets] new-wallets)))


;[2716933111 nil 22216860868 tBTCUSD 1496643016971 1496643016971 0.04 0.04 EXCHANGE LIMIT nil nil nil 0 ACTIVE nil nil 2500 0 0 0 nil nil nil 0 0 0]
(defn order->order-map [data]
  {:symbol (nth data 3) :type (nth data 8) :status (nth data 13) :amount (nth data 7)
   :price  (nth data 16) :id (nth data 0) :cid (nth data 2) :created (nth data 4)})

;(defn set-last-buy-price [cur-order {:keys [last-buy-price]}]
;  (if (< 0 (:amount cur-order)) (reset! last-buy-price (:price cur-order)) (reset! last-buy-price nil)))

;[[2716981711 nil 22821785099 tBTCUSD 1496643621796 1496643621801 -0.04 -0.04 EXCHANGE LIMIT nil nil nil 0 ACTIVE nil nil 2521 0 0 0 nil nil nil 0 0 0]]
(defn parse-orders [msg-data {:keys [personal-data] :as state}]
  (println "os: " msg-data)
  (when (not-empty (nth msg-data 2))
    (let [datas (nth msg-data 2)
          first-current-order (order->order-map (first datas))]
      ;(set-last-buy-price first-current-order state)
      (mapv (fn [data] (swap! personal-data update-in [:orders] conj (order->order-map data))) datas))))



;[2716933111 nil 22216860868 tBTCUSD 1496643016971 1496643016971 0.04 0.04 EXCHANGE LIMIT nil nil nil 0 ACTIVE nil nil 2500 0 0 0 nil nil nil 0 0 0]
(defn parse-order-new [msg-data {:keys [personal-data] :as state}]
  (println "on: " msg-data)
  (let [data (nth msg-data 2)
        order (order->order-map data)]
    (swap! personal-data update-in [:orders] conj order)))
    ;(set-last-buy-price order state)))




;[2716933111 nil 22216860868 tBTCUSD 1496643016971 1496643577580 0.04 0.04 EXCHANGE LIMIT nil nil nil 0 ACTIVE nil nil 2511 0 0 0 nil nil nil 0 0 0]
(defn parse-order-update [msg-data {:keys [personal-data] :as state}]
  (println "ou: " msg-data)
  (let [data (nth msg-data 2)
        cur-orders (:orders @personal-data)
        orders-without-updated (remove #(= (:id %) (nth data 0)) cur-orders)]
    (swap! personal-data assoc-in [:orders] orders-without-updated)
    (parse-order-new msg-data state)))


;[35784824 tBTCUSD 1496643613000 2716933111 0.04 2513.4 EXCHANGE LIMIT 2514 -1]
(defn parse-order-executed [msg-data {:keys [personal-data] :as state}]
  (println "te: " msg-data)
  (let [data (nth msg-data 2)
        cur-orders (:orders @personal-data)
        cur-orders-updated (remove #(= (:id %) (nth data 3)) cur-orders)]
    (swap! personal-data assoc-in [:orders] cur-orders-updated)
    (swap! personal-data update-in [:order-history] conj
           {:symbol (nth data 1) :type (nth data 6) :status (str "EXECUTED @ " (nth data 7) "("(nth data 4) ")")
            :amount (nth data 4) :price (nth data 7) :id (nth data 3) :created (nth data 2)})))


;[2721570796 nil 1496697142019 tBTCUSD 1496697142578 1496698075110 0.039 0.039 EXCHANGE LIMIT nil nil nil 0 CANCELED nil nil 2615 0 0 0 nil nil nil 0 0 0]
(defn parse-order-cancel [msg-data {:keys [personal-data] :as state}]
  (let [data (nth msg-data 2)
        order-canceled (order->order-map data)
        orders (:orders @personal-data)]
    (swap! personal-data assoc-in [:orders] (remove #(= (:id %) (:id order-canceled)) orders))
    (swap! personal-data update-in [:order-history] conj order-canceled)))

(defn parse-historical-orders [msg-data {:keys [personal-data] :as state}]
  (let [data (nth msg-data 2)
        orders (mapv order->order-map data)]
    (swap! personal-data assoc-in [:order-history] orders)))

(defn handle-secured-msg [db state message]
  (when (is-no-heartbeat? message)
    (let [msg-data (json/read-str message)
          ws-data (when (vector? msg-data)
                    (cond
                      (= (second msg-data) "ws") (parse-wallets msg-data state)
                      (= (second msg-data) "wu") (parse-wallet-update msg-data state)
                      ;(= (second msg-data) "ps") nil
                      (= (second msg-data) "os") (parse-orders msg-data state)
                      (= (second msg-data) "on") (parse-order-new msg-data state)
                      (= (second msg-data) "ou") (parse-order-update msg-data state)
                      (= (second msg-data) "oc") (parse-order-cancel msg-data state)
                      (= (second msg-data) "te") (parse-order-executed msg-data state)
                      (= (second msg-data) "tu") nil
                      (= (second msg-data) "hos") (parse-historical-orders msg-data state)
                      (= (second msg-data) "hts") nil
                      :else (println "unhandled msg: " msg-data)))]
      (when-not (nil? ws-data)
        (doseq [channel @state/channels]
          (async/send! channel (state/encode-transit ws-data)))))))
