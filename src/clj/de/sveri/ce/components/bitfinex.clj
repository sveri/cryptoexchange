(ns de.sveri.ce.components.bitfinex
  (:require [com.stuartsierra.component :as component]
            [de.sveri.ce.components.helper :as c-h]
            [gniazdo.core :as ws]
            [clojure.tools.logging :as l]
            [clojure.data.json :as json]
            [clj-time.core :as time-c]
            [buddy.core.mac :as mac]
            [buddy.core.codecs :as codec]
            [de.sveri.ce.remoteapi.bitfinex :as bitf]
            [immutant.scheduling :as s])
  (:import (org.joda.time DateTime)))

(def trades-socket-hearbeat-key :trades-socket-hearbeat-key)

(defrecord Bitfinex [config db]
  component/Lifecycle
  (start [component]
    (let [trades-socket (c-h/open-socket #(bitf/handle-trades-msg (:db db) %) c-h/ws-url-bitfinex)
          ;ticker-socket (c-h/open-socket #(bitf/handle-ticker-msg (:db db) %) c-h/ws-url-bitfinex)]
          _ (ws/send-msg trades-socket (json/write-str {:event "subscribe" :channel "trades" :symbol "tBTCUSD"}))
          j (s/schedule #(ws/send-msg trades-socket (json/write-str {:event "ping"})) (-> (s/id trades-socket-hearbeat-key) (s/every 30 :seconds)))]
      ;(ws/send-msg ticker-socket (json/write-str {:event "subscribe" :channel "ticker" :symbol "tBTCUSD"}))

      (assoc component :bitfinex-trades-socket trades-socket :bitfinex-trades-heartbet-sched j)))
      ;(assoc component :bitfinex-trades-socket trades-socket :bitfinex-ticker-socket ticker-socket)))
  (stop [component]
    (c-h/close-socket :bitfinex-trades-socket component)
    (s/stop (:bitfinex-trades-heartbet-sched component))
    ;(c-h/close-socket :bitfinex-ticker-socket component)
    component))

(defn new-bitfinex []
  (map->Bitfinex {}))

