(ns de.sveri.ce.components.bot-bitfinex
  (:require [com.stuartsierra.component :as component]
            [gniazdo.core :as ws]
            [clojure.tools.logging :as l]
            [clojure.data.json :as json]
            [clj-time.core :as time-c]
            [buddy.core.mac :as mac]
            [buddy.core.codecs :as codec]
            [de.sveri.ce.remoteapi.bitfinex :as bitf]
            [de.sveri.ce.remoteapi.bitfinex-trade :as bitf-t]
            [de.sveri.ce.service.bitfinex :as s-bitf]
            [de.sveri.ce.components.helper :as c-h]
            [immutant.scheduling :as s])
  (:import (org.joda.time DateTime)))


(defn generated-secured-payload [config]
  (let [api-secret (:bitfinex-api-secret (:config config))
        nonce (System/currentTimeMillis)
        payload (str "AUTH" nonce)
        sig (mac/hash payload {:key api-secret :alg :hmac+sha384})]
    (json/write-str {:event       "auth" :apiKey (:bitfinex-api-key (:config config)) :authNonce nonce
                     :authPayload payload :authSig (codec/bytes->hex sig)
                     :filter      ["trading" "wallet" "balance" "account info"]})))

(def bitfinex-bot-key :bitfinex-bot-key)
(def bitfinex-analyze-key :bitfinex-analyze-key)

(defrecord BotBitfinex [config db state]
  component/Lifecycle
  (start [component]
    (let [secured-socket (c-h/open-socket #(bitf/handle-secured-msg (:db db) (:state state) %) c-h/ws-url-bitfinex)]
      (ws/send-msg secured-socket (generated-secured-payload config))
      (swap! bitf-t/sockets assoc-in [:secured-bitfinex-socket] secured-socket)
      (s/schedule #(bitf-t/trade (:state state) (:db db)) (-> (s/id bitfinex-bot-key) (s/every 1 :seconds)))
      (s/schedule #(s-bitf/analyze (:db db)) (-> (s/id bitfinex-analyze-key) (s/every 1 :minutes))))
    component)
  (stop [component]
    (when-let [s (:secured-bitfinex-socket @bitf-t/sockets)] (ws/close s))
    (s/stop (s/id bitfinex-bot-key))
    (s/stop (s/id bitfinex-analyze-key))
    component))

(defn new-bot-bitfinex []
  (map->BotBitfinex {}))

