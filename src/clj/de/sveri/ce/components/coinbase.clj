(ns de.sveri.ce.components.coinbase
  (:require [com.stuartsierra.component :as component]
            [de.sveri.ce.components.helper :as c-h]
            [gniazdo.core :as ws]
            [clojure.tools.logging :as l]
            [clojure.data.json :as json]
            [clj-time.core :as time-c]
            [buddy.core.mac :as mac]
            [buddy.core.codecs :as codec]
            [de.sveri.ce.remoteapi.bitfinex :as bitf])
  (:import (org.joda.time DateTime)))


(defrecord Coinbase [config db]
  component/Lifecycle
  (start [component] component)
    ;(let [trades-socket (c-h/open-socket #(bitf/handle-trades-msg (:db db) %) nil)
    ;      ticker-socket (c-h/open-socket #(bitf/handle-ticker-msg (:db db) %) nil)]
    ;  (ws/send-msg trades-socket (json/write-str {:event "subscribe" :channel "trades" :symbol "tBTCUSD"}))
    ;  (ws/send-msg ticker-socket (json/write-str {:event "subscribe" :channel "ticker" :symbol "tBTCUSD"}))
    ;
    ;  (assoc component :bitfinex-trades-socket trades-socket :bitfinex-ticker-socket ticker-socket)))
  (stop [component]
    (c-h/close-socket :bitfinex-trades-socket component)
    (c-h/close-socket :bitfinex-ticker-socket component)
    component))

(defn new-coinbase []
  (map->Coinbase {}))

