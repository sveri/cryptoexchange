(ns de.sveri.ce.components.helper
  (:require [gniazdo.core :as ws]
            [clojure.tools.logging :as l]))



(def ws-url-bitfinex "wss://api.bitfinex.com/ws/2")


(def ws-url-coinbase "wss://ws-feed.gdax.com")



(defn open-socket [on-receive ws-url]
  (ws/connect ws-url
              :on-connect (fn [a] (l/info (str "opened connection to " ws-url " : " a)))
              :on-receive on-receive
              :on-error #(l/error %)
              :on-close (fn [a b] (l/info (str "closed connection to " ws-url " : " a  " " b)))))
                          ;(open-socket on-receive ws-url))))

(defn close-socket [k component]
  (when-let [s (k component)] (ws/close s)))
