(ns de.sveri.ce.db.bitfinex
  (:require [clojure.java.jdbc :as j]
            [clj-time.core :as time-c]
            [clj-time.format :as time-f])
  (:import (org.joda.time DateTimeZone)))

(defn get-trades [db last-x-minutes]
  (let [from-time (time-f/unparse (time-f/with-zone (time-f/formatters :mysql) (DateTimeZone/forID "Europe/Berlin"))
                                  (time-c/minus (time-c/now) (time-c/minutes last-x-minutes)))]
    (j/query db [(format "select * from bitfinex_trades where traded_at > '%s'" from-time)]
                {:identifiers #(.replace % \_ \-)})))


(defn get-last-buy-trade [db]
  (j/query db [(format "select * from bitfinex_trades where action = 'buy' ORDER BY id DESC LIMIT 1")]))

(defn get-last-x [db last-x]
  (j/query db ["select * from bitfinex_trades ORDER BY id DESC LIMIT ?" last-x]))

