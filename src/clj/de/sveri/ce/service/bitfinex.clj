(ns de.sveri.ce.service.bitfinex
  (:require [incanter.stats :as stats]
            [incanter.interpolation :as pol]
            [de.sveri.ce.db.bitfinex :as db-bit]
            [clj-time.coerce :as c]
            [clj-time.core :as t]
            [clj-time.core :as time-c]
            [cortex.util :as c-util]
            [cortex.nn.execute :as nn]
            [clojure.math.combinatorics :as comb]))

(def loc-db {:connection-uri "jdbc:postgresql://localhost:5432/cryptoexchange?user=cryptoexchange&password=cryptoexchange"})


(defn get-quantiles [trades]
  (let [sells (filter #(= "sell" (:action %)) trades)
        buys (filter #(= "buy" (:action %)) trades)

        sell-prices (map :price sells)
        buy-prices (map :price buys)

        quantile-sell-price (stats/quantile sell-prices)
        quantile-buy-price (stats/quantile buy-prices)]
    {:quantile-sell-price quantile-sell-price :quantile-buy-price quantile-buy-price}))




(defn get-max-min-for-last-x-minutes [db last-x-minutes]
  (let [trades (db-bit/get-trades db last-x-minutes)
        sells (filter #(= "sell" (:action %)) trades)
        buys (filter #(= "buy" (:action %)) trades)
        buy-prices (map :price buys)
        sell-prices (map :price sells)]
    {:min-buy (reduce min buy-prices) :max-buy (reduce max buy-prices) :min-sell (reduce min sell-prices) :max-sell (reduce max sell-prices)}))



(defn create-candlestick-data [trades]
  (let [sorted-trades (sort-by :trade-id trades)
        min-price (reduce #(min %1 (:price %2)) (Integer/MAX_VALUE) sorted-trades)
        max-price (reduce #(max %1 (:price %2)) 0 sorted-trades)]
    {:opened-at (:price (first sorted-trades)) :closed-at (:price (last sorted-trades))
     :min-price min-price :max-price max-price :volume (reduce + (map :amount sorted-trades))}))


(defn create-timeline-for [k candlesticks]
  (reduce (fn [acc idx] (conj acc [idx (k (nth candlesticks (dec idx)))]))
          []
          (range 1 (inc (count candlesticks)))))


(defn get-candlestick-data-for-last-x-minutes [x db]
  (let [trades (sort-by :trade-id (db-bit/get-trades db x))
        trades-split (partition-by
                       (comp
                         (juxt t/year t/month t/day t/hour t/minute)
                         c/from-date
                         :traded-at)
                       trades)]
    (mapv create-candlestick-data trades-split)))


(defn predict-with-try-catch [timeline type predict-plus-data-count]
  (try
    ((pol/interpolate timeline type) predict-plus-data-count)
    (catch Exception e 0)))



(defn create-predictions [candlestick-data history-minutes predict-for-minutes]
  (if (<= (inc predict-for-minutes) (count candlestick-data))
    (let [goal-price (:min-price (last candlestick-data))
          ;predict-for-minutes 1
          candlestick-prediction-data (subvec candlestick-data 0 (dec (count candlestick-data)))
          data-to-predict-from (subvec candlestick-data 0 (- (count candlestick-prediction-data) predict-for-minutes))
          timeline (create-timeline-for :min-price data-to-predict-from)
          predict-plus-data-count (+ predict-for-minutes (count candlestick-prediction-data))]
      {:history-minutes      history-minutes
       :predict-for-minutes predict-for-minutes
       :linear               (predict-with-try-catch timeline :linear predict-plus-data-count)
       :cubic                (predict-with-try-catch timeline :cubic predict-plus-data-count)
       :cubic-hermite        (predict-with-try-catch timeline :cubic-hermite predict-plus-data-count)
       :linear-least-squares (predict-with-try-catch timeline :linear-least-squares predict-plus-data-count)
       :goal-price           goal-price})
    {}))


(def history-minutes [10 20 30 40 60 90 120])
(def predict-minutes [1 2 3 4 5 10 20 30])

(defn analyze [db]
  (map (fn [[history-minutes predict-minutes]]
         (create-predictions (get-candlestick-data-for-last-x-minutes history-minutes db) history-minutes predict-minutes))
       (comb/cartesian-product history-minutes predict-minutes)))


(defn filter-diff-from-map [m type goal-price]
  (let [diff (- goal-price (get m type 0))]
    (if (and (< diff 5) (< 0 diff))
      (assoc m type diff)
      (dissoc m type))))


(defn figure-out-best-predictions [db]
  (let [analytic-data (analyze db)
        goal-price (:goal-price (first analytic-data))
        maps-with-diffs (map (fn [m]
                               (-> m
                                   (filter-diff-from-map :linear goal-price)
                                   (filter-diff-from-map :cubic goal-price)
                                   (filter-diff-from-map :cubic-hermite goal-price)
                                   (filter-diff-from-map :linear-least-squares goal-price)))
                             analytic-data)]
    (filter #(< 3 (count %)) maps-with-diffs)))



(def db-uri "jdbc:postgresql://localhost:5432/cryptoexchange?user=cryptoexchange&password=cryptoexchange")
(def db {:connection-uri db-uri})

(defn predict-using-nn [db]
  (let [network (c-util/read-nippy-file "trained-network.nippy")
        latest-x-buy-sells (db-bit/get-last-x db 5)
        latest-x-prices (mapv :price latest-x-buy-sells)]
    (nn/run network [{:x latest-x-prices}])))