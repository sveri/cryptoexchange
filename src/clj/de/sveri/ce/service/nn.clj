(ns de.sveri.ce.service.nn
  (:require [cortex.experiment.train :as train]
            [cortex.nn.execute :as execute]
            [cortex.nn.layers :as layers]
            [cortex.nn.network :as network]
            [de.sveri.ce.db.bitfinex :as db-bit]
            [de.sveri.ce.service.bitfinex :as s-bit]
            [clojure.java.io :as io]
            [cortex.util :as c-util]))


;(defn my-secret-fn [[x y]]
;  [(* x y)])
;
;(defn gen-random-seq-input []
;  (repeatedly (fn [] [(rand-int 10) (rand-int 10)])))
;
;(defn gen-random-seq []
;  (let [random-input (gen-random-seq-input)]
;    (map #(hash-map :x % :y (my-secret-fn %)) random-input)))
;
;
;(def teach-dataset
;  (into [] (take 20000 (gen-random-seq))))
;
;(def test-dataset
;  (into [] (take 20000 (gen-random-seq))))


(def my-network
  (network/linear-network
    [(layers/input 5 1 1 :id :x)
     (layers/linear->tanh 10)
     (layers/tanh)
     (layers/linear 1 :id :y)]))

;
;(def trained
;  (binding [*out* (io/writer "my-training.log")]
;    (train/train-n my-network teach-dataset test-dataset
;                   :batch-size 1000
;                   :network-filesystem "my-fn"
;                   :epoch-count 3000)))


;(def f "ff")


;(def trained
;  (binding [*out* (io/writer "my-training.log")]
;    (train/train-n my-network teach-dataset test-dataset
;                   :batch-size 100
;                   :network-filesystem "my-fn"
;                   :epoch-count 300)))

(defn train [network teach-dataset test-dataset]
  (binding [*out* (io/writer "my-training.log")]
    (train/train-n network teach-dataset test-dataset
                   :batch-size 1000
                   :network-filesystem "my-fn"
                   :epoch-count 3000)))


(def db-uri "jdbc:postgresql://localhost:5432/cryptoexchange?user=cryptoexchange&password=cryptoexchange")
(def db {:connection-uri db-uri})

;(defn create-teach-data [candlestick-data]
;  (loop [i 0 counter (- (count candlestick-data) 10) ret []]
;    (if (= i counter)
;      ret
;      (recur (inc i) counter
;             (conj ret {:x (vec (map :max-price (subvec candlestick-data i (+ i 5))))
;                        :y [(:max-price (nth candlestick-data (+ i 10)))]})))))


(defn create-teach-data-set-from-db-data [db-data]
  (loop [i 0 max-elems (- (count db-data) 10) ret []]
    (if (= i max-elems)
      ret
      (recur (inc i) max-elems
             (conj ret {:x (vec (map :price (subvec db-data i (+ i 5))))
                        :y [(:price (nth db-data (+ i 10)))]})))))

(defn train-with-historic-prices-amounts [db network]
  (let [db-data (vec (db-bit/get-last-x db 100000))
        db-data-prepared (vec (create-teach-data-set-from-db-data db-data))
        teach-data (take-nth 2 db-data-prepared)
        test-data (take-nth 2 (subvec db-data-prepared 1))]
    ;(println (count teach-data))))
    (train network teach-data test-data)))


(defn retrain-with-historic-prices-amounts [db]
  (let [trained-network (c-util/read-nippy-file "trained-network.nippy")]
    (train-with-historic-prices-amounts db trained-network)))
