(ns de.sveri.ce.components.state
  (:require [com.stuartsierra.component :as component]))

(defrecord State [config]
  component/Lifecycle
  (start [component]
    (let [state {:personal-data       (atom {:wallets [] :orders [] :order-history []})
                 :btc-amount-to-trade (atom 0.036)
                 :margin              (atom 7)
                 :prediction-data     (atom [])}]
      (assoc component :state state)))
  (stop [component] component))

(defn new-state []
  (map->State {}))
