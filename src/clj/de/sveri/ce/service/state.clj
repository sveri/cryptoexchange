(ns de.sveri.ce.service.state
  (:require [cognitect.transit :as t])
  (:import (java.io ByteArrayOutputStream)))





(defonce channels (atom #{}))







(defn encode-transit [message]
  (let [out (ByteArrayOutputStream. 4096)
        writer (t/writer out :json)]
    (t/write writer message)
    (.toString out)))