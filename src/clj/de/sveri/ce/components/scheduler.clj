(ns de.sveri.ce.components.scheduler
  (:require [immutant.scheduling :as s]
            [com.stuartsierra.component :as component]
            [clojure.java.jdbc :as j]
            [clj-time.core :as time-c]
            [clj-time.format :as time-f]
            [incanter.stats :as stats]
            [de.sveri.ce.db.bitfinex :as db-bit]
            [immutant.web.async :as async]
            [de.sveri.ce.service.bitfinex :as s-bit]
            [de.sveri.ce.service.state :as state])
  (:import (org.joda.time DateTimeZone)))


;(DateTimeZone/forID "Europe/Berlin")

(defn send-trade-stats-to-clients [db]
  (let [trades (db-bit/get-trades db 5)
        trade-data (merge {:type :trade-stats} (s-bit/get-quantiles trades))]
    (doseq [channel @state/channels]
      (async/send! channel (state/encode-transit trade-data)))))


(def bf-key :bitfinex-trade-stats)

(defrecord Scheduler [db]
  component/Lifecycle
  (start [component]
    (s/schedule #(send-trade-stats-to-clients (:db db)) (-> (s/id bf-key) (s/every 10 :seconds)))
    component)
  (stop [component]
    (s/stop (s/id bf-key))
    component))

(defn new-scheduler []
  (map->Scheduler {}))
