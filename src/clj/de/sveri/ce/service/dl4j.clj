(ns de.sveri.ce.service.dl4j)
  ;(:require [nd4clj.linalg.factory.nd4j :refer (zeros)]
  ;          [dl4clj.examples.example-utils :refer (index-map)]
  ;          [nd4clj.linalg.api.ndarray.indarray :refer (put-scalar get-scalar shape)]
  ;          [nd4clj.linalg.dataset.api.data-set :refer (get-features get-labels)]
  ;          [nd4clj.linalg.dataset.data-set :refer (data-set)]
  ;          [nd4clj.linalg.dataset.api.iterator.data-set-iterator :refer (reset num-examples has-next next)]
  ;          [dl4clj.nn.conf.neural-net-configuration :refer (neural-net-configuration)]
  ;          [dl4clj.examples.example-utils :refer (shakespeare)])
  ;(:import [java.util NoSuchElementException]
  ;         [java.util Random]
  ;         [java.io IOException]
  ;         [org.nd4j.linalg.dataset.api.iterator DataSetIterator]))
  ;(:require []))

;
;
;
;(def mini-batch-size 32)
;(def example-length 100)
;(def examples-per-epoch (* mini-batch-size 50))
;(def lstm-layer-size 200)
;(def num-epochs 30)
;(def generation-initialization nil)
;(def rng (Random. 12345))
;(def n-characters-to-sample 300)
;(def n-samples-to-generate 4)
;
;;; Get a DataSetIterator that handles vectorization of text into something we can use to train our
;;; GravesLSTM network.
;(def iter (get-shakespeare-iterator mini-batch-size example-length examples-per-epoch))
;
;;; Set up network configuration:
;;; broken
;(def conf (neural-net-configuration
;            {:optimization-algo :stochastic-gradient-descent
;             :iterations 1
;             :learning-rate 0.1
;             :rms-decay 0.95
;             :seed 12345
;             :regularization true
;             :l2 0.001
;             :list 3
;             :layers {0 {:graves-lstm
;                         {:n-in (input-columns iter)
;                          :n-out lstm-layer-size
;                          :updater :rmsprop
;                          :activation :tanh
;                          :weight-init :distribution
;                          :dist {:uniform {:lower -0.08, :upper 0.08}}}}
;                      1 {:graves-lstm
;                         {:n-in lstm-layer-size
;                          :n-out lstm-layer-size
;                          :updater :rmsprop
;                          :activation :tanh
;                          :weight-init :distribution
;                          :dist {:uniform {:lower -0.08, :upper 0.08}}}}
;                      2 {:rnnoutput
;                         {:loss-function :mcxent
;                          :activation :softmax
;                          :updater :rmsprop
;                          :n-in lstm-layer-size
;                          :n-out (total-outcomes iter)
;                          :weight-init :distribution
;                          :dist {:uniform {:lower -0.08, :upper 0.08}}}}}
;             :pretrain false
;             :backprop true}))
;(def net (multi-layer-network conf))
;(init net)
;;; not yet implemented:
;;; net.setListeners(new ScoreIterationListener(1));
;
;;; Print the  number of parameters in the network (and for each layer)
;(dotimes [i (count (get-layers net))]
;  (println "Number of parameters in layer "  i  ": "  (model/num-params (get-layer net i))))
;(println "Total number of network parameters: " (reduce + (map model/num-params (get-layers net))))
;
;;; Do training, and then generate and print samples from network
;(dotimes [i num-epochs]
;  (classifier/fit net iter)
;
;  (println "--------------------")
;  (println "Completed epoch " i)
;  (println "Sampling characters from network given initialization \"\"")
;  (loop [j 0
;         samples (sample-characters-from-network generation-initialization net iter rng n-characters-to-sample n-samples-to-generate)]
;    (println "----- Sample " j " -----")
;    (println (first samples))
;    (println)
;    (when-not (empty? samples)
;      (recur (inc j) (rest samples))))
;
;  ;; Reset iterator for another epoch
;  (reset iter))